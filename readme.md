## Laravel Todo

在学习laravel和angularjs中做的一个todo小应用

后端：laravel提供简单的api

前端：angularjs、bootstrap

##How to install
由于因为这是完整包，所以不需要额外去执行composer install了

1. 下载解压，配置app/config/database.php中的mysql段的数据库名(默认是laravel_todo)、用户名和密码

2. 在根目录下(artisan同级目录) 执行php artisan migrate创建数据库

3. 运行即可看到效果

### License
[MIT license](http://opensource.org/licenses/MIT)