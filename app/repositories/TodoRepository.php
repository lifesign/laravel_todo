<?php

class TodoRepository
{
    public function all() {
        return Todo::all();
    }

    public function store($input) {
        return ( $item = Todo::create($input) ) ? $item->id : 0;
    }

    public function destroy($id) {
        $todo = Todo::find($id);
        if (!$todo) {
            return false;
        }

        return $todo->delete();
    }

    public function update($id) {
        $todo = Todo::find($id);
        if (!$todo) {
            return false;
        }

        $todo->completed = !$todo->completed;
        return $todo->save();
    }
}


