<?php

class Todo extends Eloquent
{
    protected $fillable = ['title', 'completed'];

    /**
     * 强制转换成bool
     */
    public function getCompletedAttribute($value) {
        return (bool) $value;
    }
}
