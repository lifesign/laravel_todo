<?php

class TodoApiController extends BaseController
{
    protected $todoRepo;

    public function __construct(TodoRepository $todoRepo) {
        $this->todoRepo = $todoRepo;
    }

    public function index() {
        return View::make('todo');
    }

    public function show() {
        return Response::json($this->todoRepo->all());
    }

    public function update($id) {
        return Response::json($this->todoRepo->update($id));
    }

    public function store() {
        return $this->todoRepo->store(Input::get());
    }

    public function destroy($id) {
        if (!$id) {
            return false;
        }

        return Response::json($this->todoRepo->destroy($id));
    }
}

