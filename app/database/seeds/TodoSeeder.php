<?php

#todo数据填充
class TodoSeeder extends Seeder
{
    public function run() {
        #删除数据先
        DB::table('todos')->delete();

        Todo::create(array('title' => '学习angular', 'completed' => false));
        Todo::create(array('title' => '预习大规模网站架构', 'completed' => false));
        Todo::create(array('title' => '公司的活', 'completed' => true));

    }
}

