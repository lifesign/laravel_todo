<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


#todo api
Route::group(['prefix' => 'api'], function(){
    #resource controller
    Route::resource('todo', 'TodoApiController');
    # list
    Route::get('todos', 'TodoApiController@show');
    #version
    Route::get('version', function(){return '1.0';});
});