<!DOCTYPE html>
<html ng-app="laravelTodo">
    <title>Laravel Todo List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <!-- Bootstrap -->
    {{HTML::style('assets/css/bootstrap.min.css')}}
    {{HTML::style('assets/css/style.css')}}
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/2.0.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{HTML::script('assets/js/bootstrap.min.js')}}

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js"></script>
    {{HTML::script('assets/js/app.js')}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container" ng-controller="TodoCtrl">
        <div class="row">
            <div class="col-12">
                <h2>今日待做事项 <%currentTime|date:'H:mm:ss a'%></h2>
                <input type="search" placeholder="Search" ng-model="searchBox">
                <hr>
                <table class="table table-hover table-bordered table-responsive">
                    <tbody>
                        <tr>
                            <form ng-submit="addItem()">
                                <div class="form-group">
                                    <input class="form-control" type="text" ng-model="itemEntry" placeholder="请输入待做事件 回车结束" autofocus>
                                </div>
                            </form>
                        </tr>
                        <tr>
                            <!-- 进度条 -->
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="<%progressbarItem()%>" aria-valuemin="0" aria-valuemax="100" style="width: <%progressbarItem()%>%;">
                                    <%completedTodo()%> / <%countItem()%></span>
                                </div>
                            </div>
                        </tr>
                        <tr class="<%isDone(item.completed)%>" ng-repeat="item in items | filter:searchBox">
                            <td>
                                <input type="checkbox" id="item-<%item.id%>" ng-model="item.completed" ng-click="updateStatus(item.id)" >
                                <label for="item-<%item.id%>" class="<%isDone(item.completed)%>"><%item.title%><span></span></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped table-hover">
                    <tbody>
                        <tr>
                            <button class="btn btn-primary btn-lg btn-block btn-danger" ng-click="clearTodo()">清除已完成事项</button>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- /col-12>-->
        </div> <!-- /row -->
    </div> <!-- /container -->
</body>
</html>